---
AWSTemplateFormatVersion: "2010-09-09"
Description: "Creates RDS Service"
Parameters:
  VPCID:
    Description : "VPC Id"
    Type        : AWS::EC2::VPC::Id

  EnvironmentAbbr:
    Description : Environment abbreviation used to generate DNS names
    Type        : String

  SliceName:
    Description : The name of the cluster without env prefix
    Type        : String

  DatabaseName:
    Type         : String
  DatabaseUser:
    Type         : String

  BackupRetentionPeriod:
    Description  : The number of days during which automatic DB snapshots are retained. If set to 0, no backup will be created at the created of the RDS
    Type         : String
    Default      : 32

  RDSName:
    Type         : String
  RDSSubnets:
    Description  : Comma seperated list of database subnets in the VPC
    Type         : List<AWS::EC2::Subnet::Id>
  RDSMultiAZ:
    Type         : String
  RDSAllocatedStorage:
    Type         : Number
  RDSInstanceType:
    Type         : String
  RDSMasterUsername:
    Type         : String
  RDSVersion:
    Type         : String
  RDSEncrypt:
    Type         : Number
    AllowedValues: [0, 1]
    Default      : 1
  RDSParameterGroupName:
    Type         : String

  SourceDBInstanceIdentifier:
    Type         : String
  RDSSnapShotName:
    Type         : String

Conditions:
  CreateRDSResourcesFromSnapshot   : !Not [ !Equals [ !Ref RDSSnapShotName, '' ] ]
  IsReadReplica                    : !Not [ !Equals [ !Ref SourceDBInstanceIdentifier, '' ] ]
  IsMaster                         : !Equals [ !Ref SourceDBInstanceIdentifier, '' ]
  NotCreateRDSResourcesFromSnapshot: !Equals [ !Ref RDSSnapShotName, '' ]
  CNRDSEncrypt                     : !Equals [ !Ref RDSEncrypt, 1 ]

Resources:

  #####################################################################
  #                                                                   #
  # RDS                                                               #
  #                                                                   #
  #####################################################################

  DBSubnetGroup:
    Type: "AWS::RDS::DBSubnetGroup"
    Properties:
      DBSubnetGroupDescription: Created from the RDS Management Console
      SubnetIds:
        Ref: RDSSubnets

  RDSSG:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: Services RDS
      VpcId: !Ref VPCID
      Tags:
        - Key: Name
          Value: !Sub "${AWS::StackName}-db"

  RDSSGInLambdaResource:
    Type: "AWS::EC2::SecurityGroupIngress"
    Properties:
      GroupId: !Ref RDSSG
      IpProtocol: "tcp"
      FromPort:
        Fn::If: [ IsMaster, !GetAtt PostgresRDS.Endpoint.Port, !GetAtt PostgresRDSReplica.Endpoint.Port ]
      ToPort:
        Fn::If: [ IsMaster, !GetAtt PostgresRDS.Endpoint.Port, !GetAtt PostgresRDSReplica.Endpoint.Port ]
      SourceSecurityGroupId:
        Fn::ImportValue: !Sub "lambda-${EnvironmentAbbr}-cfndbprovider-LambdaSGID"

  RDSMasterPassword:
    Type: "Custom::Secret"
    Properties:
      ServiceToken: !Sub "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:CFNSecretProvider"
      Name        : !Sub "/${EnvironmentAbbr}/${SliceName}/${DatabaseName}/rds_password"
      Alphabet    : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      ReturnSecret: true

  DBUserPassword:
    Type: "Custom::Secret"
    Properties:
      ServiceToken: !Sub "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:CFNSecretProvider"
      Name        : !Sub "/${EnvironmentAbbr}/${SliceName}/${DatabaseName}/database_password"
      Alphabet    : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
      ReturnSecret: true

  DBUser:
    Type: "Custom::PostgresDBUser"
    Condition: "NotCreateRDSResourcesFromSnapshot"
    Properties:
      ServiceToken: !Sub "arn:aws:lambda:${AWS::Region}:${AWS::AccountId}:function:${EnvironmentAbbr}-CFNDBProvider"
      User        : !Ref DatabaseUser
      Password    : !GetAtt DBUserPassword.Secret
      Database    :
        Password: !GetAtt RDSMasterPassword.Secret
        User    : !Ref RDSMasterUsername
        Host:
          Fn::If: [ IsMaster, !GetAtt PostgresRDS.Endpoint.Address, !GetAtt PostgresRDSReplica.Endpoint.Address ]
        Port:
          Fn::If: [ IsMaster, !GetAtt PostgresRDS.Endpoint.Port, !GetAtt PostgresRDSReplica.Endpoint.Port ]
        DBName  : !Ref DatabaseName

  PostgresRDSReplica:
    Type: "AWS::RDS::DBInstance"
    Condition: IsReadReplica
    Properties:
      DBInstanceIdentifier: !Ref RDSName
      SourceDBInstanceIdentifier: !Ref SourceDBInstanceIdentifier
      DBSnapshotIdentifier:
        Fn::If:
          - CreateRDSResourcesFromSnapshot
          - !Ref RDSSnapShotName
          - !Ref AWS::NoValue
      AllocatedStorage: !Ref RDSAllocatedStorage
      StorageType     : "gp2"
      DBInstanceClass : !Ref RDSInstanceType
      Engine          : "postgres"
      EngineVersion   : !Ref RDSVersion
      DBParameterGroupName: !Ref RDSParameterGroupName

      AllowMajorVersionUpgrade: false
      AutoMinorVersionUpgrade : true
      CopyTagsToSnapshot      : true
      PreferredMaintenanceWindow: "mon:04:01-mon:06:01"

      DBSubnetGroupName: !Ref DBSubnetGroup
      VPCSecurityGroups:
        - !Ref RDSSG

      Tags:
        - Key: Name
          Value: !Sub "${AWS::StackName}"

  PostgresRDS:
    Type: "AWS::RDS::DBInstance"
    Condition: IsMaster
    Properties:
      DBName:
        Fn::If:
          - CreateRDSResourcesFromSnapshot
          - !Ref AWS::NoValue
          - !Ref DatabaseName
      DBInstanceIdentifier: !Ref RDSName
      DBSnapshotIdentifier:
        Fn::If:
          - CreateRDSResourcesFromSnapshot
          - !Ref RDSSnapShotName
          - !Ref AWS::NoValue
      MasterUsername: !Ref RDSMasterUsername
      MasterUserPassword: !GetAtt RDSMasterPassword.Secret
      MultiAZ: !Ref RDSMultiAZ
      AllocatedStorage: !Ref RDSAllocatedStorage
      StorageType     : "gp2"
      StorageEncrypted:
        Fn::If:
          - CNRDSEncrypt
          - true
          - !Ref "AWS::NoValue"
      DBInstanceClass : !Ref RDSInstanceType
      Engine          : "postgres"
      EngineVersion   : !Ref RDSVersion
      DBParameterGroupName: !Ref RDSParameterGroupName

      AllowMajorVersionUpgrade  : false
      AutoMinorVersionUpgrade   : true
      CopyTagsToSnapshot        : true
      BackupRetentionPeriod     : !Ref BackupRetentionPeriod
      PreferredBackupWindow     : "02:00-04:00"
      PreferredMaintenanceWindow: "mon:04:01-mon:06:01"

      DBSubnetGroupName: !Ref DBSubnetGroup
      VPCSecurityGroups:
        - !Ref RDSSG

      Tags:
        - Key: Name
          Value: !Sub "${AWS::StackName}"

  #####################################################################
  #                                                                   #
  # SSM Parameter passing for Docker tasks                            #
  #                                                                   #
  #####################################################################
  SSMParamDBUser:
    Type: "AWS::SSM::Parameter"
    Properties:
      Name: !Sub "/${EnvironmentAbbr}/${SliceName}/${DatabaseName}/database_user"
      Description: "The user used by the application to connect to the database"
      Type: "String"
      Value: !Ref DatabaseUser

  SSMParamDBName:
    Type: "AWS::SSM::Parameter"
    Properties:
      Name: !Sub "/${EnvironmentAbbr}/${SliceName}/${DatabaseName}/database_name"
      Description: "The database name that the application connects to"
      Type: "String"
      Value: !Ref DatabaseName

  SSMParamDBHost:
    Type: "AWS::SSM::Parameter"
    Properties:
      Name: !Sub "/${EnvironmentAbbr}/${SliceName}/${DatabaseName}/rds_host"
      Description: "RDS Hostname that the application connects to"
      Type: "String"
      Value:
        Fn::If: [ IsMaster, !GetAtt PostgresRDS.Endpoint.Address, !GetAtt PostgresRDSReplica.Endpoint.Address ]

  SSMParamDBPort:
    Type: "AWS::SSM::Parameter"
    Properties:
      Name: !Sub "/${EnvironmentAbbr}/${SliceName}/${DatabaseName}/rds_port"
      Description: "RDS port that the application connects to"
      Type: "String"
      Value:
        Fn::If: [ IsMaster, !GetAtt PostgresRDS.Endpoint.Port, !GetAtt PostgresRDSReplica.Endpoint.Port ]

Outputs:
  RDSSG:
    Value: !Ref RDSSG
    Export:
      Name: !Sub "${AWS::StackName}-RDSSG"
  RDSHost:
    Value:
      Fn::If: [ IsMaster, !GetAtt PostgresRDS.Endpoint.Address, !GetAtt PostgresRDSReplica.Endpoint.Address ]
    Export:
      Name: !Sub "${AWS::StackName}-DBHost"
  RDSPort:
    Value:
      Fn::If: [ IsMaster, !GetAtt PostgresRDS.Endpoint.Port, !GetAtt PostgresRDSReplica.Endpoint.Port ]
    Export:
      Name: !Sub "${AWS::StackName}-DBPort"
